#include <iostream>
#include <string>

const int N = 10;

void evenOdd(int odd);

int main()
{
	

	for (int i = 0; i <= N; i+=2) 
	{		
			std::cout << i << std::endl;		
	}	

	evenOdd(0);
	evenOdd(1);
}

void evenOdd(int odd) {
	for (int i = odd; i <= N; i += 2) 
	{
			std::cout << i << std::endl;		
	}
}
